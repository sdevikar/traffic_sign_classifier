Traffic Sign Classifier Project Report
=
**Prepared by: Swapnil Devikar**

Loading and examining the data
-
Training and testing data pickle files were placed in a separate folder _traffic-signs-data_ and loaded from there. The statistics are as follows:

| Dataset   |      Count      |  Dimensions |
|----------|:-------------:|------:|
| Training |  39209 | (32,32,3) |
| Testing |    12630   |   (32,32,3) |
| Labels | 43| (1,43)

This was to make sure that the images are proper dimension.
In addition, a histogram was plotted to visualize how images are distributed in terms of their labels.

To make sure that images are being properly loaded, the following cell picks a random image and displays it in its original size.

Design and Test a Model Architecture
-
##Preprocessing the data
Several options were considered for preprocessing the data. Including:
1. Rotating the image
2. Gray-scaling the image
3. Or gray-scaling and using those images in addition to the RGB images (maybe convert to gray-scale and stack)
4. Normalization
5. Scaling the images as per the sizes and x, y co-ordinates

For validation during the training process, 20% of the training data was reserved. The training data and labels were shuffled, each time the data is drawn and split from the training set. As a final sanity check, the image was displayed again and checked against the corresponding label index. The label index was manually looked up in the .csv file to make sure that the image corresponds to a valid label.

Two ideas from the initial list of pre-processing ideas were experimented with. Namely:
1. Generating Fake data:
This approach sounded promising at first, because it would double the size of training dataset and also give a gray-scaled alternative for colored training images. Although, in the end, this approach did not yield much better results. Possible causes could be:
    - It'd skew the dataset towards the images that are already gray-scaled. i.e. more gray-scaled images than colored
    - Color of the sign could be a good indicator of whether it's a warning. hazard (yellow and red) or just an informative sign (exit, traveller info etc.)
2. Normalization of gray-scaled images: This approach did not yield any significant performance gain either.

##Architecture
Starting from LeNet as a base, a few layers were added for performance improvement. Including two dropout layers to prevent overfitting. The overall architecture looks as follows:
![Architecture](tsc_arch.png)

## Training
- **Optimizer:**
    - AdamOptimizer was used for training purposes. AdamOptimizer was used to minimize the cross entropy. The steps followed were as follows:
    - Calculated cross entropy with logits
    - loss was calculated using mean value of cross entropy

- **batch size:**
    - The batch size = 128 was used for training purpose. This decision was made because AWS EC2 with GPU was fast enough to run one epoch in under a minute.

- **number of epochs**
    - Started testing with 10 epochs and gradually increased the epochs to 30, after iterative testing.
    - After the dropout layers were added, the accuracy of the model dropped on the validation dataset. For that reason, I increased to epoch size to 50, to get the model to process the images at ~96% accuracy.

- **hyperparameters**
    - **Learning rate:** Played around with the learning rates, starting from 0.1 to 0.0001 (additional explanation is in the answer to question#5) Finally, a **learning rate of 0.001 was finalized**
    - **Epochs:** As discussed above, epoch size was set to 50
    - **Filter dimensions:**
        - Convolutional layer1: Kernel size = 5x5, output depth = 6
        - Convolutional layer2: Kernel size = 5x5, output depth = 16
        - Convolutional layer3: Kernel size = 2x2, output depth = 32
        - Convolutional layer4: Kernel size = 2x2, output depth = 64
        - Strides for all these kernels was set to 1
        - In all the maxpool layers, kernel size = 2
        - Keep probability for dropout layers was set to 75%
        - Mean and standard deviation for random weight generator was set to mu = 0 sigma = 0.1 respectively

## Finalizing the model
An iterative approach was taken as follows:
- Started with splitting training data as training and validation data with 80% to 20% ratio
- Fed this randomized data to the LenNet network from the lab and found out that the model offers 94% accuracy
- From this point, multiple experiments were done. As follows:
    - Generated "fake" data, by gray-scaling all the images and appending to the existing training dataset. The decision to append the gray-scaled data to color data instead of just gray-scaling everything was deliberate. This was with an understanding that the color of the traffic sign is a category in itself. e.g. a red colored sign will indicate caution, whereas green colored sign may just be information. This approach offered about 0.5% improvement in accuracy on an average.
    - When the training data size was doubled, I also increased the number of epochs to 30. This was done because, now there was more data, hence, possibility of the model to "memorize" the training dataset is reduced
    - tweaked the learning rate. Changing leaning rate to 0.1 gave bad results and it appeared that number of epochs were not enough for learning rate to 0.0001. Also experimented with learning rates such as 0.005, 0.0075 but did not see improvements. At this point, accuracy was around 96%
- Now, for regularization, a dropout layers were added after convolution layer 2. This reduced the accuracy to around 93%
- To counter for the reduced accuracy, two additional convolutional layers were added and one dropout layer was added. Input and output dimensions were adjusted accordingly.
- After I started getting consistent validation accuracy and was confident about the accuracy of the model, I went on to test it with the test data. At this point, I found out that:
    - I actually got better test accuracy when normalization and additional "fake" data was removed. So, I went ahead and removed those provisions.
    - I still got better accuracy for validation and training data, both, after removing normalization and "fake" data with 50 epochs. So, that change was retained.

##Testing the Model on New Images
**Testing data**

The finalized model was then used to classify the testing data. This is the data, the model has never seen before.
Model was able to classify the test data with 88.1% accuracy.

**USA Traffic Sign Images**

For this part, following steps were followed:
- Downloaded some US traffic signs from [Here](http://mutcd.fhwa.dot.gov/services/publications/fhwaop02084/)
- Since these images were not size 32x32. All of these images had a white space with the traffic sign description at the bottom, so the images were cropped and scaled accordingly.
- These images are for USA traffic signs. The model was trained against German traffic signs and therefore, not expected to perform with high accuracy.
- The 5 test images against which the model will be evaluated were plotted for visualization. The final accuracy after testing against the USA traffic sign images was 40%.
- The result is discussed in detail as a part of answer to Question 8 in the notebook.
